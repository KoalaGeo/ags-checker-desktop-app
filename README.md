# ags-checker-desktop-app

- HEAD is at (gitlab.com/ags-data-format-wg/ags-checker-desktop-app)[https://gitlab.com/ags-data-format-wg/ags-checker-desktop-app.git]

## Notes

This validator uses the beta version of the AGS4 validator library.
It validates 4.0.3, 4.0.4 and 4.1 format versions only.
It validates against the rules and data dictionary.

It is Beta only and should not be used to validate AGS4 in a working environment.

Please use the "log a bug" button to record problems or wish list items. You can also use the discussion forum to discuss issues (login rqd).

This software was created Asitha Senanayake with assistance from Roger Chandler and Tony Daly,
Thanks to all the beta testers.

The front end has been written using PySimpleGUI [pysimplegui](https://pysimplegui.readthedocs.io/en/latest/)
The code for the validator library and desktop application is on the AGS data format working group, GitLab site [AGS library](https://gitlab.com/ags-data-format-wg)

The project is on GitLab [here](https://gitlab.com/ags-data-format-wg/ags-checker-desktop-app)

## Licence

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public Licence as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public Licence for more details.

## Installation and running

This build of the software has been tested on Windows 10 only.
It is a desktop-only application.

No istallation is required. The zip file should be extracted to a suitable folder on the system and the .exe file double-clicked.
Python runtime libraries are included in the installation folder, Python does not need to be installed on the client PC.

The user-interface is shown below:

![User interface](assets/ui.png)

