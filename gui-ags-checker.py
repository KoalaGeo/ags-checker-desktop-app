#
# This file is part of the ags-checker-desktop-app distribution.
# https://gitlab.com/ags-data-format-wg/ags-checker-desktop-app
# Copyright (c) 2021 AGS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import PySimpleGUI as sg
import os.path
from python_ags4 import AGS4
import webbrowser

def print_to_screen(ags_errors):
    '''Print error report to screen.'''

    error_count = 0
    for key, val in ags_errors.items():
        if 'Rule' in key:
            error_count += len(val)

    # Print  metadata
    if 'Metadata' in ags_errors.keys():
        for entry in ags_errors['Metadata']:
            cprint(f'''{entry['line']}: \t {entry['desc']}''')
        cprint('')

    # Summary of errors log
    if error_count == 0:
        cprint('All checks passed.\n')
    else:
        cprint(f'{error_count} error(s) found in file.\n')

    # Print 'General' error messages first if present
    if 'General' in ags_errors.keys():
        cprint('General:')

        for entry in ags_errors['General']:
            cprint(f'''  {entry['desc']}''')
        cprint('')

    # Print other error messages
    for key in [x for x in ags_errors if 'Rule' in x]:
        cprint("FAIL: "f'''{key}:''')
        for entry in ags_errors[key]:
            cprint(f'''  Line {entry['line']}\t {entry['group'].strip('"')}\t {entry['desc']}''')
        cprint('')

##################################################################
# GUI
##################################################################
gui_version = 'Beta v0.1.1'
sg.ChangeLookAndFeel('SystemDefault')

cprint = sg.cprint
version_choices = ['4.0.3', '4.0.4', '4.1.0']

layout = [[sg.Text('AGS Validator', font=('Arial', 16))],

          [sg.Text('Select AGS v4 file for validation:'),
           sg.Input(key='_INTXT_', background_color='yellow'),
           sg.FileBrowse(file_types=(("AGS files", "*.ags"),), key='_IN_', enable_events=True)],
          [sg.HorizontalSeparator()], #-----------------------------------------------
          [sg.Text('AGS Version (default 4.0.4):', tooltip='the specific AGS version may be specified in the TRAN group'),
           sg.Radio('4.0.3', 1, size=(12, 1), enable_events=True, key='_VERSION403_'),
           sg.Radio('4.0.4', 1, size=(12, 1), enable_events=True, key='_VERSION404_', default=True),
           sg.Radio('4.1.0', 1, size=(12, 1), enable_events=True, key='_VERSION410_')],
          [sg.HorizontalSeparator()], #-----------------------------------------------
          [sg.Button(button_text='Validate File', enable_events=True, key='_CHECK_', size=(20, 2)),],
          [sg.HorizontalSeparator()], #-----------------------------------------------
          [sg.Text('Validation output log:')],
          [sg.Multiline(size=(100, 15), key='_OUT_PRINT_', default_text='output area', disabled=True, enable_events=True,
                        auto_refresh=True)],
          [sg.HorizontalSeparator()], #-----------------------------------------------
          [sg.Text('Export file path for validation report:'),
           sg.Input(key='_OUTTXT_'),
           sg.FileSaveAs(button_text='Browse', file_types=(("txt files", "*.txt"),), key='_OUT_')],
          [sg.Button(button_text='Export Validation Report', enable_events=True, key='_EXPORT_')],
          [sg.HorizontalSeparator()], #-----------------------------------------------
          [sg.Text('Convert ags file to xlsx', font=('Arial', 14))],
          [sg.Text('Export file path for spreadsheet file:'),
           sg.Input(key='_XLSXTXT_'),
           sg.FileSaveAs(button_text='Browse', file_types=(("xlsx files", "*.xlsx"),), key='_OUT_XLSX_')],
          [sg.Button(button_text='Export AGS file to xlsx', enable_events=True, key='_XLSX_')],
          [sg.HorizontalSeparator()], #-----------------------------------------------
          [sg.Exit(), sg.Button(button_text='Help', enable_events=True, key='_HELP_'),
           sg.Button(button_text='Log a bug', enable_events=True, key='_WEB_PAGE_',tooltip='launches the AGS web site, please fill in the form'),
           sg.Button(button_text='Discussion Forum', enable_events=True, key='_DISC_PAGE_',tooltip='launches the AGS web site, Discussion forum, login rqd')]
          ]

window = sg.Window('AGS File Validator - ' + gui_version, layout, font=("Helvetica", 11), resizable=True, finalize=True,
                   icon=r'assets/AGS.ico')

window.bind('<Configure>', "Configure") # used to detect window maximize

sg.cprint_set_output_destination(window, '_OUT_PRINT_')

while True:  # Event Loop
    event, values = window.read()

    if event == "Configure":
        window['_OUT_PRINT_'].expand(True, True,True)
        window['_INTXT_'].expand(True, False, False)
        window['_OUTTXT_'].expand(True, False, False)
        window['_XLSXTXT_'].expand(True, False, False)

    if event == '_CHECK_':
        if os.path.isfile(values['_INTXT_']) is False:
            sg.Popup('No ags file selected', auto_close=True, auto_close_duration=5)
        elif os.path.isfile(values['_INTXT_']) is True:
            window.FindElement('_OUT_PRINT_').Update('')
            # cprint('Validating file, please wait')
            window.FindElement('_OUT_PRINT_').Update('')
            cprint('AGS validation report')
            cprint('File to be validated: ' + values['_INTXT_'])
            cprint('Validation carried out using a beta version of the desktop software ' + gui_version)
            if values['_VERSION404_'] == True:
                cprint('validation using Standard_dictionary_v4_0_4.ags')
            elif values['_VERSION410_'] == True:
                cprint('validation using Standard_dictionary_v4_1_0.ags')
            elif values['_VERSION403_'] == True:
                cprint('validation using Standard_dictionary_v4_0_3.ags')

            cprint('---------------------------------------------------------------------------------')
            cprint('Validation Started...This could take some time for a large file.')
            cprint('')
            # try:
            if values['_VERSION404_'] == True:
                print_to_screen(AGS4.check_file(values['_INTXT_'], "dicts/Standard_dictionary_v4_0_4.ags"))
            elif values['_VERSION410_'] == True:
                print_to_screen(AGS4.check_file(values['_INTXT_'], "dicts/Standard_dictionary_v4_1_0.ags"))
            elif values['_VERSION403_'] == True:
                print_to_screen(AGS4.check_file(values['_INTXT_'], "dicts/Standard_dictionary_v4_0_3.ags"))
            cprint('---------------------------------------------------------------------------------')
            cprint('Validation finished')
            # TODO include here: option to leave version to file - use check.py code to pull version.

        # if event == '_CANCELVAL_':
        #     #sys.exit('terminated')
        #     #TODO cancel not working - need to put check in thread and end thread.
        #     print('terminate')
        #     raise SystemExit

    if event == '_EXPORT_':

        name_file = values['_OUTTXT_']
        path_exp = os.path.dirname(values['_OUTTXT_'])
        txt = values['_OUT_PRINT_']
        print(txt)

        if os.path.exists(path_exp) is False:
            sg.Popup("The folder doesn't exist, please choose another one", auto_close=True, auto_close_duration=5)

        elif os.path.exists(path_exp) is True:

            if 'output area' in txt:
                sg.Popup('Please run the validation, nothing to export', auto_close=True, auto_close_duration=5)
                print('output: ' + txt)

            if 'output area' not in txt and values['_OUTTXT_'] != '':
                try:
                    fh = open(name_file, 'w+')

                except FileNotFoundError:
                    fh = open(name_file, 'w+')
                    sg.Popup('File Not found')

                fh.write(values['_OUT_PRINT_'])
                fh.close()
                sg.Popup('Export complete')

    if event == '_XLSX_':
        path_xls = os.path.dirname(values['_XLSXTXT_'])
        if os.path.exists(path_xls) is False:
            sg.Popup("The folder doesn't exist, please choose another one", auto_close=True, auto_close_duration=5)
        elif os.path.exists(path_xls) is True:
            try:
                sg.popup_no_buttons('Export starting, please wait', auto_close=True, auto_close_duration=5)
                AGS4.AGS4_to_excel(values['_INTXT_'], values['_XLSXTXT_'])
                sg.Popup('Export complete')
            except IOError as x:
                if x.errno == 13:
                    sg.Popup('The xlsx file in use, please close the file and retry')

    if event == '_HELP_':
        print('link to the help on the web site?')
        print(values['_OUT_PRINT_'])
        sg.PopupScrolled('AGS 4 Validator help, Version ' + gui_version,
                         '',
                         '- This validator uses the beta version of the AGS4 validator library',
                         '- It validates 4.0.3, 4.0.4 and 4.1 format versions only',
                         '- It validates against the rules and data dictionary',
                         '',
                         '- It is Beta only and should not be used to validate AGS 4 in a working environment.',
                         '',
                         '- Please use the "log a bug" button to record problems or wish list items. You can also use the discussion forum to discuss issues (login rqd)',
                         '- This software was created Asitha Senanayake with assistance from Roger Chandler and Tony Daly,',
                         '- Thanks to all the beta testers',
                         '- The front end has been written using PySimpleGUI',
                         '- https://pysimplegui.readthedocs.io/en/latest/',
                         '- The code for the validator library and desktop application is on',
                         '- the AGS data format working group, GitLab site https://gitlab.com/ags-data-format-wg',
                         '',
                         'Licence',
                         '',
                         'This program is free software: you can redistribute it and/or modify',
                         'it under the terms of the GNU General Public Licence as published by',
                         'the Free Software Foundation, version 3.',
                         '',
                         'This program is distributed in the hope that it will be useful, but',
                         'WITHOUT ANY WARRANTY; without even the implied warranty of',
                         'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public Licence for more details.')

    if event == '_WEB_PAGE_':
        webbrowser.open_new('https://www.ags.org.uk/data-format/ags-validator/')

    if event == '_DISC_PAGE_':
        webbrowser.open_new('https://www.ags.org.uk/data-format/dwqa-questions/ ')

    if event in (None, 'Exit'):
        break

window.close()
